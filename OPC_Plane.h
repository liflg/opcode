///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 *  Contains code for planes.
 *  \file   IcePlane.h
 *  \author   Pierre Terdiman
 *  \date   April, 4, 2000
 */
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma once

#define PLANE_EPSILON (1.0e-7f)

namespace IceMaths {
class ICEMATHS_API Plane {
public:
  //! Constructor
  Plane() : n(), d(0.0f) {}
  //! Constructor
  Plane(float nx, float ny, float nz, float d) : n(), d(0.0f) {
    Set(nx, ny, nz, d);
  }
  //! Constructor
  Plane(const Point &p, const Point &n) : n(), d(0.0f) { Set(p, n); }
  //! Constructor
  Plane(const Point &p0, const Point &p1, const Point &p2) : n(), d(0.0f) {
    Set(p0, p1, p2);
  }
  //! Constructor
  Plane(const Point &n, float d) : n(n), d(d) {}
  //! Copy constructor
  Plane(const Plane &plane) : n(plane.n), d(plane.d) {}
  //! Destructor
  ~Plane() {}

  Plane &Zero() {
    n.Zero();
    d = 0.0f;
    return *this;
  }
  Plane &Set(float nx, float ny, float nz, float d) {
    n.Set(nx, ny, nz);
    this->d = d;
    return *this;
  }
  Plane &Set(const Point &p, const Point &n) {
    this->n = n;
    d = -p | n;
    return *this;
  }
  Plane &Set(const Point &p0, const Point &p1, const Point &p2);

  float Distance(const Point &p) const { return (p | n) + d; }
  bool Belongs(const Point &p) const {
    return fabsf(Distance(p)) < PLANE_EPSILON;
  }

  void Normalize() {
    float Denom = 1.0f / n.Magnitude();
    n.x *= Denom;
    n.y *= Denom;
    n.z *= Denom;
    d *= Denom;
  }

public:
  // Members
  Point n; //!< The normal to the plane
  float d; //!< The distance from the origin

  // Cast operators
  operator Point() const { return n; }
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 *  Transforms a plane by a 4x4 matrix. Same as Plane * Matrix4x4 operator,
 *but faster.
 *  \param    transformed [out] transformed plane
 *  \param    plane   [in] source plane
 *  \param    transform [in] transform matrix
 */
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
inline void TransformPlane(Plane &transformed, const Plane &plane,
                           const Matrix4x4 &transform) {
  // Catch the rotation part of the 4x4 matrix
  Matrix3x3 Rot = transform;

  // Rotate the normal
  transformed.n = plane.n * Rot;

  // Compute new d
  Point Trans;
  transform.GetTrans(Trans);
  transformed.d = (plane.d * transformed.n - Trans) | transformed.n;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 *  Transforms a plane by a 4x4 matrix. Same as Plane * Matrix4x4 operator,
 *but faster.
 *  \param    plane   [in/out] source plane (transformed on
 *return)
 *  \param    transform [in] transform matrix
 */
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
inline void TransformPlane(Plane &plane, const Matrix4x4 &transform) {
  // Catch the rotation part of the 4x4 matrix
  Matrix3x3 Rot = transform;

  // Rotate the normal
  plane.n *= Rot;

  // Compute new d
  Point Trans;
  transform.GetTrans(Trans);
  plane.d = (plane.d * plane.n - Trans) | plane.n;
}
}
