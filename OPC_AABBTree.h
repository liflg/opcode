///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *  OPCODE - Optimized Collision Detection
 *  Copyright (C) 2001 Pierre Terdiman
 *  Homepage: http://www.codercorner.com/Opcode.htm
 */
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 *  Contains code for a versatile AABB tree.
 *  \file   OPC_AABBTree.h
 *  \author   Pierre Terdiman
 *  \date   March, 20, 2001
 */
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma once

namespace Opcode {
class OPCODE_API AABBTreeNode {
public:
  /* Constructor */
  AABBTreeNode();
  /* Destructor */
  virtual ~AABBTreeNode();
  /* Data access */
  const AABB *GetAABB() const { return &mBV; }
  const AABBTreeNode *GetPos() const { return mP; }
  const AABBTreeNode *GetNeg() const { return mN; }

  bool IsLeaf() const { return (!mP && !mN); }

  /* Stats */
  uint32_t GetNodeSize() const { return SIZEOFOBJECT; }

public:
  AABBTreeNode(const AABBTreeNode &) = delete;
  const AABBTreeNode &operator=(const AABBTreeNode &) = delete;
  // Data access
  const uint32_t *GetPrimitives() const { return mNodePrimitives; }
  uint32_t GetNbPrimitives() const { return mNbPrimitives; }

protected:
  // Tree-independent data
  // Following data always belong to the BV-tree, regardless of what the tree
  // actually contains. Whatever happens we need the two children and the
  // enclosing volume.
  AABB mBV; //! Global bounding-volume enclosing all the node-related primitives
  AABBTreeNode *mP;
  AABBTreeNode *mN;

  // Tree-dependent data
  uint32_t *mNodePrimitives; //!< Node-related primitives (shortcut to a
                             //! position in mIndices below)
  uint32_t
      mNbPrimitives; //!< Number of primitives for this node Internal methods
  uint32_t Split(uint32_t axis, AABBTreeBuilder *builder);
  bool Subdivide(AABBTreeBuilder *builder);
  void buildHierarchy(AABBTreeBuilder *builder);
};

class OPCODE_API AABBTree : public AABBTreeNode {
public:
  // Constructor / Destructor
  AABBTree();
  AABBTree(const AABBTree &) = delete;
  const AABBTree &operator=(const AABBTree &) = delete;
  virtual ~AABBTree();
  // Build
  bool Build(AABBTreeBuilder *builder);
  // Data access
  const uint32_t *GetIndices() const { return mIndices; } //!< Catch the indices
  uint32_t GetNbNodes() const {
    return mTotalNbNodes;
  } //!< Catch the number of nodes

  // Infos
  bool IsComplete() const;
  // Stats
  uint32_t ComputeDepth() const;
  uint32_t GetUsedBytes() const;

private:
  uint32_t *mIndices; //!< Indices in the app list. Indices are reorganized
                      //! during build.
  // Stats
  uint32_t mTotalNbNodes; //!< Number of nodes in the tree.
};
}
