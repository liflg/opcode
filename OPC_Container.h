///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 *  Contains a simple container class.
 *  \file   IceContainer.h
 *  \author   Pierre Terdiman
 *  \date   February, 5, 2000
 */
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma once

#include "Opcode.h"

#define CONTAINER_STATS

namespace IceCore {
class ICECORE_API Container {
public:
  // Constructor / Destructor
  Container();
  Container(uint32_t size, float growth_factor);
  Container(const Container &) = delete;
  virtual ~Container();
  // Management
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  /**
   *  A O(1) method to add a value in the container. The container is
   *automatically resized if needed.
   *  The method is inline, not the resize. The call overhead happens on
   *resizes only, which is not a problem since the resizing operation
   *  costs a lot more than the call overhead...
   *
   *  \param    entry   [in] a uint32_t to store in the container
   *  \see    Add(float entry)
   *  \see    Empty()
   *  \see    Contains(uint32_t entry)
   *  \return   Self-Reference
   */
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  Container &Add(uint32_t entry) {
    // Resize if needed
    if (mCurNbEntries == mMaxNbEntries)
      Resize();

    // Add new entry
    mEntries[mCurNbEntries++] = entry;
    return *this;
  }

  Container &Add(uint64_t entry) { return Add(static_cast<uint32_t>(entry)); }

  Container &Add(const uint32_t *entries, uint32_t nb) {
    // Resize if needed
    if (mCurNbEntries + nb > mMaxNbEntries)
      Resize(nb);

    // Add new entry
    CopyMemory(&mEntries[mCurNbEntries], entries, nb * sizeof(uint32_t));
    mCurNbEntries += nb;
    return *this;
  }

  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  /**
   *  A O(1) method to add a value in the container. The container is
   *automatically resized if needed.
   *  The method is inline, not the resize. The call overhead happens on
   *resizes only, which is not a problem since the resizing operation
   *  costs a lot more than the call overhead...
   *
   *  \param    entry   [in] a float to store in the container
   *  \see    Add(uint32_t entry)
   *  \see    Empty()
   *  \see    Contains(uint32_t entry)
   *  \return   Self-Reference
   */
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  Container &Add(float entry) {
    // Resize if needed
    if (mCurNbEntries == mMaxNbEntries)
      Resize();

    // Add new entry
    mEntries[mCurNbEntries++] = IR(entry);
    return *this;
  }

  Container &Add(const float *entries, uint32_t nb) {
    // Resize if needed
    if (mCurNbEntries + nb > mMaxNbEntries)
      Resize(nb);

    // Add new entry
    CopyMemory(&mEntries[mCurNbEntries], entries, nb * sizeof(float));
    mCurNbEntries += nb;
    return *this;
  }

  //! Add unique [slow]
  Container &AddUnique(uint32_t entry) {
    if (!Contains(entry))
      Add(entry);
    return *this;
  }

  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  /**
   *  Clears the container. All stored values are deleted, and it frees used
   *ram.
   *  \see    Reset()
   *  \return   Self-Reference
   */
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  Container &Empty() {
#ifdef CONTAINER_STATS
    mUsedRam -= mMaxNbEntries * sizeof(uint32_t);
#endif
    DELETEARRAY(mEntries);
    mCurNbEntries = mMaxNbEntries = 0;
    return *this;
  }

  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  /**
   *  Resets the container. Stored values are discarded but the buffer is kept
   *so that further calls don't need resizing again.
   *  That's a kind of temporal coherence.
   *  \see    Empty()
   */
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  void Reset() {
    // Avoid the write if possible
    // ### CMOV
    if (mCurNbEntries)
      mCurNbEntries = 0;
  }

  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  /**
   *  Sets the initial size of the container. If it already contains
   *something, it's discarded.
   *  \param    nb    [in] Number of entries
   *  \return   true if success
   */
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  bool SetSize(uint32_t nb);

  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  /**
   *  Refits the container and get rid of unused bytes.
   *  \return   true if success
   */
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  bool Refit();

  // Checks whether the container already contains a given value.
  bool Contains(uint32_t entry, uint32_t *location = nullptr) const;
  // Deletes an entry - doesn't preserve insertion order.
  bool Delete(uint32_t entry);
  // Deletes an entry - does preserve insertion order.
  bool DeleteKeepingOrder(uint32_t entry);
  //! Deletes the very last entry.
  void DeleteLastEntry() {
    if (mCurNbEntries)
      mCurNbEntries--;
  }
  //! Deletes the entry whose index is given
  void DeleteIndex(uint32_t index) {
    mEntries[index] = mEntries[--mCurNbEntries];
  }

  // Helpers
  Container &FindNext(uint32_t &entry, bool wrap = false);
  Container &FindPrev(uint32_t &entry, bool wrap = false);
  // Data access.
  uint32_t GetNbEntries() const {
    return mCurNbEntries;
  } //!< Returns the current number of entries.
  uint32_t GetEntry(uint32_t i) const {
    return mEntries[i];
  } //!< Returns ith entry
  uint32_t *GetEntries() const {
    return mEntries;
  } //!< Returns the list of entries.

  // Growth control
  float GetGrowthFactor() const {
    return mGrowthFactor;
  } //!< Returns the growth factor
  void SetGrowthFactor(float growth) {
    mGrowthFactor = growth;
  } //!< Sets the growth factor

  //! Access as an array
  uint32_t &operator[](uint32_t i) const {
    assert(i < mCurNbEntries);
    return mEntries[i];
  }

  // Stats
  uint32_t GetUsedRam() const;

  //! Operator for Container A = Container B
  const Container &operator=(const Container &object) {
    SetSize(object.GetNbEntries());
    CopyMemory(mEntries, object.GetEntries(), mMaxNbEntries * sizeof(uint32_t));
    mCurNbEntries = mMaxNbEntries;
    return *this;
  }

#ifdef CONTAINER_STATS
  uint32_t GetNbContainers() const { return mNbContainers; }
  uint32_t GetTotalBytes() const { return mUsedRam; }

private:
  static uint32_t mNbContainers; //!< Number of containers around
  static uint32_t
      mUsedRam; //!< Amount of bytes used by containers in the system
#endif
private:
  // Resizing
  bool Resize(uint32_t needed = 1);
  // Data
  uint32_t mMaxNbEntries; //!< Maximum possible number of entries
  uint32_t mCurNbEntries; //!< Current number of entries
  uint32_t *mEntries;     //!< List of entries
  float mGrowthFactor;    //!< Resize: new number of entries = old number *
  //! mGrowthFactor
};

class ICECORE_API Pairs : public Container {
public:
  // Constructor / Destructor
  Pairs() {}
  ~Pairs() {}

  uint32_t GetNbPairs() const { return GetNbEntries() >> 1; }
  Pair *GetPairs() const { return (Pair *)GetEntries(); }

  Pairs &AddPair(const Pair &p) {
    Add(p.id0).Add(p.id1);
    return *this;
  }
};
}
