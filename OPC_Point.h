///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 *  Contains code for 3D vectors.
 *  \file   IcePoint.h
 *  \author   Pierre Terdiman
 *  \date   April, 4, 2000
 */
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma once

enum PointComponent { X = 0, Y = 1, Z = 2, W = 3, FORCE_DWORD = 0x7fffffff };

namespace IceMaths {
// Forward declarations
class HPoint;
class Plane;
class Matrix3x3;
class Matrix4x4;

#define CROSS2D(a, b) (a.x * b.y - b.x * a.y)

const float EPSILON2 = 1.0e-20f;

class ICEMATHS_API Point {
public:
  //! Empty constructor
  Point() : x(0.0f), y(0.0f), z(0.0f) {}
  //! Constructor from a single float
  Point(float val) : x(val), y(val), z(val) {}
  //! Constructor from floats
  Point(float _x, float _y, float _z) : x(_x), y(_y), z(_z) {}
  //! Constructor from array
  Point(float f[3]) : x(f[X]), y(f[Y]), z(f[Z]) {}
  //! Copy constructor
  Point(const Point &p) : x(p.x), y(p.y), z(p.z) {}
  //! Destructor
  ~Point() {}

  //! Clears the vector
  Point &Zero() {
    x = y = z = 0.0f;
    return *this;
  }

  //! + infinity
  Point &SetPlusInfinity() {
    x = y = z = MAX_FLOAT;
    return *this;
  }
  //! - infinity
  Point &SetMinusInfinity() {
    x = y = z = MIN_FLOAT;
    return *this;
  }

  //! Sets positive unit random vector
  Point &PositiveUnitRandomVector();
  //! Sets unit random vector
  Point &UnitRandomVector();

  //! Assignment from values
  Point &Set(float _x, float _y, float _z) {
    x = _x;
    y = _y;
    z = _z;
    return *this;
  }
  //! Assignment from array
  Point &Set(float f[3]) {
    x = f[X];
    y = f[Y];
    z = f[Z];
    return *this;
  }
  //! Assignment from another point
  Point &Set(const Point &src) {
    x = src.x;
    y = src.y;
    z = src.z;
    return *this;
  }

  //! Adds a vector
  Point &Add(const Point &p) {
    x += p.x;
    y += p.y;
    z += p.z;
    return *this;
  }
  //! Adds a vector
  Point &Add(float _x, float _y, float _z) {
    x += _x;
    y += _y;
    z += _z;
    return *this;
  }
  //! Adds a vector
  Point &Add(float f[3]) {
    x += f[X];
    y += f[Y];
    z += f[Z];
    return *this;
  }
  //! Adds vectors
  Point &Add(const Point &p, const Point &q) {
    x = p.x + q.x;
    y = p.y + q.y;
    z = p.z + q.z;
    return *this;
  }

  //! Subtracts a vector
  Point &Sub(const Point &p) {
    x -= p.x;
    y -= p.y;
    z -= p.z;
    return *this;
  }
  //! Subtracts a vector
  Point &Sub(float _x, float _y, float _z) {
    x -= _x;
    y -= _y;
    z -= _z;
    return *this;
  }
  //! Subtracts a vector
  Point &Sub(float f[3]) {
    x -= f[X];
    y -= f[Y];
    z -= f[Z];
    return *this;
  }
  //! Subtracts vectors
  Point &Sub(const Point &p, const Point &q) {
    x = p.x - q.x;
    y = p.y - q.y;
    z = p.z - q.z;
    return *this;
  }

  //! this = -this
  Point &Neg() {
    x = -x;
    y = -y;
    z = -z;
    return *this;
  }
  //! this = -a
  Point &Neg(const Point &a) {
    x = -a.x;
    y = -a.y;
    z = -a.z;
    return *this;
  }

  //! Multiplies by a scalar
  Point &Mult(float s) {
    x *= s;
    y *= s;
    z *= s;
    return *this;
  }

  //! this = a * scalar
  Point &Mult(const Point &a, float scalar) {
    x = a.x * scalar;
    y = a.y * scalar;
    z = a.z * scalar;
    return *this;
  }

  //! this = a + b * scalar
  Point &Mac(const Point &a, const Point &b, float scalar) {
    x = a.x + b.x * scalar;
    y = a.y + b.y * scalar;
    z = a.z + b.z * scalar;
    return *this;
  }

  //! this = this + a * scalar
  Point &Mac(const Point &a, float scalar) {
    x += a.x * scalar;
    y += a.y * scalar;
    z += a.z * scalar;
    return *this;
  }

  //! this = a - b * scalar
  Point &Msc(const Point &a, const Point &b, float scalar) {
    x = a.x - b.x * scalar;
    y = a.y - b.y * scalar;
    z = a.z - b.z * scalar;
    return *this;
  }

  //! this = this - a * scalar
  Point &Msc(const Point &a, float scalar) {
    x -= a.x * scalar;
    y -= a.y * scalar;
    z -= a.z * scalar;
    return *this;
  }

  //! this = a + b * scalarb + c * scalarc
  Point &Mac2(const Point &a, const Point &b, float scalarb, const Point &c,
              float scalarc) {
    x = a.x + b.x * scalarb + c.x * scalarc;
    y = a.y + b.y * scalarb + c.y * scalarc;
    z = a.z + b.z * scalarb + c.z * scalarc;
    return *this;
  }

  //! this = a - b * scalarb - c * scalarc
  Point &Msc2(const Point &a, const Point &b, float scalarb, const Point &c,
              float scalarc) {
    x = a.x - b.x * scalarb - c.x * scalarc;
    y = a.y - b.y * scalarb - c.y * scalarc;
    z = a.z - b.z * scalarb - c.z * scalarc;
    return *this;
  }

  //! this = mat * a
  Point &Mult(const Matrix3x3 &mat, const Point &a);

  //! this = mat1 * a1 + mat2 * a2
  Point &Mult2(const Matrix3x3 &mat1, const Point &a1, const Matrix3x3 &mat2,
               const Point &a2);

  //! this = this + mat * a
  Point &Mac(const Matrix3x3 &mat, const Point &a);

  //! this = transpose(mat) * a
  Point &TransMult(const Matrix3x3 &mat, const Point &a);

  //! Linear interpolate between two vectors: this = a + t * (b - a)
  Point &Lerp(const Point &a, const Point &b, float t) {
    x = a.x + t * (b.x - a.x);
    y = a.y + t * (b.y - a.y);
    z = a.z + t * (b.z - a.z);
    return *this;
  }

  //! Hermite interpolate between p1 and p2. p0 and p3 are used for finding
  //! gradient at p1 and p2.
  //! this =  p0 * (2t^2 - t^3 - t)/2
  //!     + p1 * (3t^3 - 5t^2 + 2)/2
  //!     + p2 * (4t^2 - 3t^3 + t)/2
  //!     + p3 * (t^3 - t^2)/2
  Point &Herp(const Point &p0, const Point &p1, const Point &p2,
              const Point &p3, float t) {
    float t2 = t * t;
    float t3 = t2 * t;
    float kp0 = (2.0f * t2 - t3 - t) * 0.5f;
    float kp1 = (3.0f * t3 - 5.0f * t2 + 2.0f) * 0.5f;
    float kp2 = (4.0f * t2 - 3.0f * t3 + t) * 0.5f;
    float kp3 = (t3 - t2) * 0.5f;
    x = p0.x * kp0 + p1.x * kp1 + p2.x * kp2 + p3.x * kp3;
    y = p0.y * kp0 + p1.y * kp1 + p2.y * kp2 + p3.y * kp3;
    z = p0.z * kp0 + p1.z * kp1 + p2.z * kp2 + p3.z * kp3;
    return *this;
  }

  //! this = rotpos * r + linpos
  Point &Transform(const Point &r, const Matrix3x3 &rotpos,
                   const Point &linpos);

  //! this = trans(rotpos) * (r - linpos)
  Point &InvTransform(const Point &r, const Matrix3x3 &rotpos,
                      const Point &linpos);

  //! Returns std::min(x, y, z);
  float Min() const { return std::min(x, std::min(y, z)); }
  //! Returns std::max(x, y, z);
  float Max() const { return std::max(x, std::max(y, z)); }
  //! Sets each element to be componentwise minimum
  Point &Min(const Point &p) {
    x = std::min(x, p.x);
    y = std::min(y, p.y);
    z = std::min(z, p.z);
    return *this;
  }
  //! Sets each element to be componentwise maximum
  Point &Max(const Point &p) {
    x = std::max(x, p.x);
    y = std::max(y, p.y);
    z = std::max(z, p.z);
    return *this;
  }

  //! Clamps each element
  Point &Clamp(float min, float max) {
    if (x < min)
      x = min;
    if (x > max)
      x = max;
    if (y < min)
      y = min;
    if (y > max)
      y = max;
    if (z < min)
      z = min;
    if (z > max)
      z = max;
    return *this;
  }

  //! Computes square magnitude
  float SquareMagnitude() const { return x * x + y * y + z * z; }
  //! Computes magnitude
  float Magnitude() const { return sqrtf(x * x + y * y + z * z); }
  //! Computes volume
  float Volume() const { return x * y * z; }

  //! Checks the point is near zero
  bool ApproxZero() const { return SquareMagnitude() < EPSILON2; }

  //! Slighty moves the point
  void Tweak(uint32_t coordmask, uint32_t tweakmask) {
    if (coordmask & 1) {
      uint32_t Dummy = IR(x);
      Dummy ^= tweakmask;
      x = FR(Dummy);
    }
    if (coordmask & 2) {
      uint32_t Dummy = IR(y);
      Dummy ^= tweakmask;
      y = FR(Dummy);
    }
    if (coordmask & 4) {
      uint32_t Dummy = IR(z);
      Dummy ^= tweakmask;
      z = FR(Dummy);
    }
  }

#define TWEAKMASK 0x3fffff
#define TWEAKNOTMASK ~TWEAKMASK
  //! Slighty moves the point out
  void TweakBigger() {
    uint32_t Dummy = (IR(x) & TWEAKNOTMASK);
    if (!isNegativeFloat(x))
      Dummy += TWEAKMASK + 1;
    x = FR(Dummy);
    Dummy = (IR(y) & TWEAKNOTMASK);
    if (!isNegativeFloat(y))
      Dummy += TWEAKMASK + 1;
    y = FR(Dummy);
    Dummy = (IR(z) & TWEAKNOTMASK);
    if (!isNegativeFloat(z))
      Dummy += TWEAKMASK + 1;
    z = FR(Dummy);
  }

  //! Slighty moves the point in
  void TweakSmaller() {
    uint32_t Dummy = (IR(x) & TWEAKNOTMASK);
    if (isNegativeFloat(x))
      Dummy += TWEAKMASK + 1;
    x = FR(Dummy);
    Dummy = (IR(y) & TWEAKNOTMASK);
    if (isNegativeFloat(y))
      Dummy += TWEAKMASK + 1;
    y = FR(Dummy);
    Dummy = (IR(z) & TWEAKNOTMASK);
    if (isNegativeFloat(z))
      Dummy += TWEAKMASK + 1;
    z = FR(Dummy);
  }

  //! Normalizes the vector
  Point &Normalize() {
    float M = x * x + y * y + z * z;
    if (M) {
      M = 1.0f / sqrtf(M);
      x *= M;
      y *= M;
      z *= M;
    }
    return *this;
  }

  //! Sets vector length
  Point &SetLength(float length) {
    float NewLength = length / Magnitude();
    x *= NewLength;
    y *= NewLength;
    z *= NewLength;
    return *this;
  }

  //! Computes distance to another point
  float Distance(const Point &b) const {
    return sqrtf((x - b.x) * (x - b.x) + (y - b.y) * (y - b.y) +
                 (z - b.z) * (z - b.z));
  }

  //! Computes square distance to another point
  float SquareDistance(const Point &b) const {
    return ((x - b.x) * (x - b.x) + (y - b.y) * (y - b.y) +
            (z - b.z) * (z - b.z));
  }

  //! Dot product dp = this|a
  float Dot(const Point &p) const { return p.x * x + p.y * y + p.z * z; }

  //! Cross product this = a x b
  Point &Cross(const Point &a, const Point &b) {
    x = a.y * b.z - a.z * b.y;
    y = a.z * b.x - a.x * b.z;
    z = a.x * b.y - a.y * b.x;
    return *this;
  }

  //! Returns largest axis
  PointComponent LargestAxis() const {
    const float *Vals = &x;
    PointComponent m = X;
    if (Vals[Y] > Vals[m])
      m = Y;
    if (Vals[Z] > Vals[m])
      m = Z;
    return m;
  }

  //! Returns smallest axis
  PointComponent SmallestAxis() const {
    const float *Vals = &x;
    PointComponent m = X;
    if (Vals[Y] < Vals[m])
      m = Y;
    if (Vals[Z] < Vals[m])
      m = Z;
    return m;
  }

  //! Refracts the point
  Point &Refract(const Point &eye, const Point &n, float refractindex,
                 Point &refracted);

  //! Projects the point onto a plane
  Point &ProjectToPlane(const Plane &p);

  //! Projects the point onto the screen
  void ProjectToScreen(float halfrenderwidth, float halfrenderheight,
                       const Matrix4x4 &mat, HPoint &projected) const;

  //! Hash function from Ville Miettinen
  uint32_t GetHashValue() const {
    const uint32_t *h = (const uint32_t *)(this);
    uint32_t f = (h[0] + h[1] * 11 - (h[2] * 17)) &
                 0x7fffffff; // avoid problems with +-0
    return (f >> 22) ^ (f >> 12) ^ (f);
  }

  // Arithmetic operators

  //! Unary operator for Point Negate = - Point
  Point operator-() const { return Point(-x, -y, -z); }

  //! Operator for Point Plus = Point + Point.
  Point operator+(const Point &p) const {
    return Point(x + p.x, y + p.y, z + p.z);
  }
  //! Operator for Point Minus = Point - Point.
  Point operator-(const Point &p) const {
    return Point(x - p.x, y - p.y, z - p.z);
  }

  //! Operator for Point Mul   = Point * Point.
  Point operator*(const Point &p) const {
    return Point(x * p.x, y * p.y, z * p.z);
  }
  //! Operator for Point Scale = Point * float.
  Point operator*(float s) const { return Point(x * s, y * s, z * s); }
  //! Operator for Point Scale = float * Point.
  friend Point operator*(float s, const Point &p) {
    return Point(s * p.x, s * p.y, s * p.z);
  }

  //! Operator for Point Div   = Point / Point.
  Point operator/(const Point &p) const {
    return Point(x / p.x, y / p.y, z / p.z);
  }
  //! Operator for Point Scale = Point / float.
  Point operator/(float s) const {
    s = 1.0f / s;
    return Point(x * s, y * s, z * s);
  }
  //! Operator for Point Scale = float / Point.
  friend Point operator/(float s, const Point &p) {
    return Point(s / p.x, s / p.y, s / p.z);
  }

  //! Operator for float DotProd = Point | Point.
  float operator|(const Point &p) const { return x * p.x + y * p.y + z * p.z; }
  //! Operator for Point VecProd = Point ^ Point.
  Point operator^(const Point &p) const {
    return Point(y * p.z - z * p.y, z * p.x - x * p.z, x * p.y - y * p.x);
  }

  //! Operator for Point += Point.
  Point &operator+=(const Point &p) {
    x += p.x;
    y += p.y;
    z += p.z;
    return *this;
  }
  //! Operator for Point += float.
  Point &operator+=(float s) {
    x += s;
    y += s;
    z += s;
    return *this;
  }

  //! Operator for Point -= Point.
  Point &operator-=(const Point &p) {
    x -= p.x;
    y -= p.y;
    z -= p.z;
    return *this;
  }
  //! Operator for Point -= float.
  Point &operator-=(float s) {
    x -= s;
    y -= s;
    z -= s;
    return *this;
  }

  //! Operator for Point *= Point.
  Point &operator*=(const Point &p) {
    x *= p.x;
    y *= p.y;
    z *= p.z;
    return *this;
  }
  //! Operator for Point *= float.
  Point &operator*=(float s) {
    x *= s;
    y *= s;
    z *= s;
    return *this;
  }

  //! Operator for Point /= Point.
  Point &operator/=(const Point &p) {
    x /= p.x;
    y /= p.y;
    z /= p.z;
    return *this;
  }
  //! Operator for Point /= float.
  Point &operator/=(float s) {
    s = 1.0f / s;
    x *= s;
    y *= s;
    z *= s;
    return *this;
  }

  // Logical operators

  //! Operator for "if(Point==Point)"
  bool operator==(const Point &p) const {
    return ((IR(x) == IR(p.x)) && (IR(y) == IR(p.y)) && (IR(z) == IR(p.z)));
  }
  //! Operator for "if(Point!=Point)"
  bool operator!=(const Point &p) const {
    return ((IR(x) != IR(p.x)) || (IR(y) != IR(p.y)) || (IR(z) != IR(p.z)));
  }

  // Arithmetic operators

  //! Operator for Point Mul = Point * Matrix3x3.
  Point operator*(const Matrix3x3 &mat) const {
    class ShadowMatrix3x3 {
    public:
      float m[3][3];
    }; // To allow inlining
    const ShadowMatrix3x3 *Mat = (const ShadowMatrix3x3 *)&mat;

    return Point(x * Mat->m[0][0] + y * Mat->m[1][0] + z * Mat->m[2][0],
                 x * Mat->m[0][1] + y * Mat->m[1][1] + z * Mat->m[2][1],
                 x * Mat->m[0][2] + y * Mat->m[1][2] + z * Mat->m[2][2]);
  }

  //! Operator for Point Mul = Point * Matrix4x4.
  Point operator*(const Matrix4x4 &mat) const {
    class ShadowMatrix4x4 {
    public:
      float m[4][4];
    }; // To allow inlining
    const ShadowMatrix4x4 *Mat = (const ShadowMatrix4x4 *)&mat;

    return Point(
        x * Mat->m[0][0] + y * Mat->m[1][0] + z * Mat->m[2][0] + Mat->m[3][0],
        x * Mat->m[0][1] + y * Mat->m[1][1] + z * Mat->m[2][1] + Mat->m[3][1],
        x * Mat->m[0][2] + y * Mat->m[1][2] + z * Mat->m[2][2] + Mat->m[3][2]);
  }

  //! Operator for Point *= Matrix3x3.
  Point &operator*=(const Matrix3x3 &mat) {
    class ShadowMatrix3x3 {
    public:
      float m[3][3];
    }; // To allow inlining
    const ShadowMatrix3x3 *Mat = (const ShadowMatrix3x3 *)&mat;

    float xp = x * Mat->m[0][0] + y * Mat->m[1][0] + z * Mat->m[2][0];
    float yp = x * Mat->m[0][1] + y * Mat->m[1][1] + z * Mat->m[2][1];
    float zp = x * Mat->m[0][2] + y * Mat->m[1][2] + z * Mat->m[2][2];

    x = xp;
    y = yp;
    z = zp;

    return *this;
  }

  //! Operator for Point *= Matrix4x4.
  Point &operator*=(const Matrix4x4 &mat) {
    class ShadowMatrix4x4 {
    public:
      float m[4][4];
    }; // To allow inlining
    const ShadowMatrix4x4 *Mat = (const ShadowMatrix4x4 *)&mat;

    float xp =
        x * Mat->m[0][0] + y * Mat->m[1][0] + z * Mat->m[2][0] + Mat->m[3][0];
    float yp =
        x * Mat->m[0][1] + y * Mat->m[1][1] + z * Mat->m[2][1] + Mat->m[3][1];
    float zp =
        x * Mat->m[0][2] + y * Mat->m[1][2] + z * Mat->m[2][2] + Mat->m[3][2];

    x = xp;
    y = yp;
    z = zp;

    return *this;
  }

  // Cast operators

  //! Cast a Point to a HPoint. w is set to zero.
  operator HPoint() const;

  operator const float *() const { return &x; }
  operator float *() { return &x; }

public:
  float x;
  float y;
  float z;
};
FUNCTION ICEMATHS_API void Normalize1(Point &a);
FUNCTION ICEMATHS_API void Normalize2(Point &a);
}
