///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 *  Contains custom types.
 *  \file   IceTypes.h
 *  \author   Pierre Terdiman
 *  \date   April, 4, 2000
 */
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma once

// Constants
#define PI 3.1415926535897932384626433832795028841971693993751f //!<  PI
#define INV3 0.33333333333333333333f                            //!< 1/3

//! references
#define INVALID_ID                                                             \
  0xffffffff //!< Invalid dword ID (counterpart of nullptr pointers)

//! A generic couple structure
struct Pair {
  uint32_t id0; //!<  First index of the pair
  uint32_t id1; //!<  Second index of the pair
  Pair() : id0(0), id1(0) {}
  virtual ~Pair() {}
};

// Type ranges
#define MAX_FLOAT FLT_MAX         //!<  max possible float value
#define MIN_FLOAT (-FLT_MAX)      //!<  min possible loat value
#define IEEE_1_0 0x3f800000       //!<  integer representation of 1.0
#define IEEE_MAX_FLOAT 0x7f7fffff //!<  integer representation of MAX_FLOAT

#define ONE_OVER_RAND_MAX                                                      \
  (1.0f /                                                                      \
   float(RAND_MAX)) //!<  Inverse of the max possible value returned by rand()

#define MAXMAX(a, b, c)                                                        \
  ((a) > (b) ? std::max(a, c)                                                  \
             : std::max(b, c)) //!<  Returns the max value between a, b and c
