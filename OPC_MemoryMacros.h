///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 *  Contains all memory macros.
 *  \file   IceMemoryMacros.h
 *  \author   Pierre Terdiman
 *  \date   April, 4, 2000
 */
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma once

#undef ZeroMemory
#undef CopyMemory
#undef MoveMemory
#undef FillMemory

#include <cstring>

//! Clears a buffer.
//! \param    addr  [in] buffer address
//! \param    size  [in] buffer length
//! \see    FillMemory
//! \see    StoreDwords
//! \see    CopyMemory
//! \see    MoveMemory
inline void ZeroMemory(void *addr, uint32_t size) { memset(addr, 0, size); }

//! Fills a buffer with a given byte.
//! \param    addr  [in] buffer address
//! \param    size  [in] buffer length
//! \param    val   [in] the byte value
//! \see    StoreDwords
//! \see    ZeroMemory
//! \see    CopyMemory
//! \see    MoveMemory
inline void FillMemory(void *dest, uint32_t size, uint8_t val) {
  memset(dest, val, size);
}

//! Copies a buffer.
//! \param    addr  [in] destination buffer address
//! \param    addr  [in] source buffer address
//! \param    size  [in] buffer length
//! \see    ZeroMemory
//! \see    FillMemory
//! \see    StoreDwords
//! \see    MoveMemory
inline void CopyMemory(void *dest, const void *src, uint32_t size) {
  memcpy(dest, src, size);
}

//! Moves a buffer.
//! \param    addr  [in] destination buffer address
//! \param    addr  [in] source buffer address
//! \param    size  [in] buffer length
//! \see    ZeroMemory
//! \see    FillMemory
//! \see    StoreDwords
//! \see    CopyMemory
inline void MoveMemory(void *dest, const void *src, uint32_t size) {
  memmove(dest, src, size);
}

#define SIZEOFOBJECT                                                           \
  sizeof(*this) //!< Gives the size of current object. Avoid some mistakes (e.g.
                //!"sizeof(this)").
//#define CLEAROBJECT   { memset(this, 0, SIZEOFOBJECT);  }
////!< Clears current object. Laziness is my business. HANDLE WITH CARE.
#define DELETESINGLE(x)                                                        \
  if (x) {                                                                     \
    delete x;                                                                  \
    x = nullptr;                                                               \
  } //!< Deletes an instance of a class.
#define DELETEARRAY(x)                                                         \
  if (x) {                                                                     \
    delete[] x;                                                                \
    x = nullptr;                                                               \
  } //!< Deletes an array.
#define SAFE_RELEASE(x)                                                        \
  if (x) {                                                                     \
    (x)->Release();                                                            \
    (x) = nullptr;                                                             \
  } //!< Safe D3D-style release
#define SAFE_DESTRUCT(x)                                                       \
  if (x) {                                                                     \
    (x)->SelfDestruct();                                                       \
    (x) = nullptr;                                                             \
  } //!< Safe ICE-style release

#define CHECKALLOC(x)                                                          \
  if (!x)                                                                      \
    return false;

//! Standard allocation cycle
#define SAFE_ALLOC(ptr, type, count)                                           \
  DELETEARRAY(ptr);                                                            \
  ptr = new type[count];                                                       \
  CHECKALLOC(ptr);
