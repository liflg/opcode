///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 *  Contains code to compute the minimal bounding sphere.
 *  \file   IceBoundingSphere.h
 *  \author   Pierre Terdiman
 *  \date   January, 29, 2000
 */
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma once

namespace Meshmerizer {
class MESHMERIZER_API Sphere {
public:
  //! Constructor
  Sphere() : mCenter(), mRadius() {}
  //! Constructor
  Sphere(const Point &center, float radius)
      : mCenter(center), mRadius(radius) {}
  //! Constructor
  Sphere(const uint32_t n, Point *p);
  //! Copy constructor
  Sphere(const Sphere &sphere)
      : mCenter(sphere.mCenter), mRadius(sphere.mRadius) {}
  //! Destructor
  ~Sphere() {}

  bool Compute(uint32_t nbverts, Point *verts);
  bool FastCompute(uint32_t nbverts, Point *verts);

  // Access methods
  const Point &GetCenter() const { return mCenter; }
  float GetRadius() const { return mRadius; }

  const Point &Center() const { return mCenter; }
  float Radius() const { return mRadius; }

  Sphere &Set(const Point &center, float radius) {
    mCenter = center;
    mRadius = radius;
    return *this;
  }
  Sphere &SetCenter(const Point &center) {
    mCenter = center;
    return *this;
  }
  Sphere &SetRadius(float radius) {
    mRadius = radius;
    return *this;
  }

  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  /**
   *  Tests if a point is contained within the sphere.
   *  \param    p [in] the point to test
   *  \return   true if inside the sphere
   */
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  bool Contains(const Point &p) const {
    return mCenter.SquareDistance(p) < mRadius * mRadius;
  }

  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  /**
   *  Tests if a sphere is contained within the sphere.
   *  \param    sphere  [in] the sphere to test
   *  \return   true if inside the sphere
   */
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  bool Contains(const Sphere &sphere) const {
    float r = mRadius - sphere.mRadius;
    return mCenter.SquareDistance(sphere.mCenter) < r * r;
  }

  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  /**
   *  Tests if a box is contained within the sphere.
   *  \param    aabb  [in] the box to test
   *  \return   true if inside the sphere
   */
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  bool Contains(const AABB &aabb) const {
    // I assume if all 8 box vertices are inside the sphere, so does the whole
    // box.
    // Sounds ok but maybe there's a better way?
    float R2 = mRadius * mRadius;
    Point Max;
    aabb.GetMax(Max);
    Point Min;
    aabb.GetMin(Min);

    Point p;
    p.x = Max.x;
    p.y = Max.y;
    p.z = Max.z;
    if (mCenter.SquareDistance(p) >= R2)
      return false;
    p.x = Min.x;
    if (mCenter.SquareDistance(p) >= R2)
      return false;
    p.x = Max.x;
    p.y = Min.y;
    if (mCenter.SquareDistance(p) >= R2)
      return false;
    p.x = Min.x;
    if (mCenter.SquareDistance(p) >= R2)
      return false;
    p.x = Max.x;
    p.y = Max.y;
    p.z = Min.z;
    if (mCenter.SquareDistance(p) >= R2)
      return false;
    p.x = Min.x;
    if (mCenter.SquareDistance(p) >= R2)
      return false;
    p.x = Max.x;
    p.y = Min.y;
    if (mCenter.SquareDistance(p) >= R2)
      return false;
    p.x = Min.x;
    if (mCenter.SquareDistance(p) >= R2)
      return false;

    return true;
  }

public:
  Point mCenter; //!< Sphere center
  float mRadius; //!< Sphere radius
};
}
